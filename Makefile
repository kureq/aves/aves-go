# vim: ft=make

#? Environment ------------------------------------------------------------------------------------------------------->
SHELL 	:= /usr/bin/env bash
OS 		:= $(shell { uname -s | tr '[:upper:]' '[:lower:]' 2>/dev/null || echo "unknown"; })
ARCH 	:= $(shell { uname -m | tr '[:upper:]' '[:lower:]' 2>/dev/null || echo "unknown"; })
OUT_DIR := ./out/

ifeq ($(ARCH), x86_64)
	ARCH := amd64
endif

LIB_EXTENSION := $(shell { [ "$(OS)" == "darwin" ] && echo "dylib" || echo "so"; })
#? End Environment --------------------------------------------------------------------------------------------------->

.PHONY: build-linux
build-linux:
	@docker buildx build --platform=linux/amd64 --output type=local,dest=$(OUT_DIR) -f Dockerfile.linux .
	@printf "Created release file, Size %s, Path %s\n" $$(du -sh $(OUT_DIR)/aves-$(OS)-$(ARCH).$(LIB_EXTENSION))

.PHONY: build
build: build-linux 

.PHONY: download
download: $(LOCAL_DIR)
	@git clone https://git.savannah.gnu.org/git/bash.git $(LOCAL_DIR)/bash

