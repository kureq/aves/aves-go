#include "env.h"
#include "variables.h"
#include <stdio.h>

void free_env_vars(CEnv *env_vars, int count) {
  for (int i = 0; i < count; i++) {
    free(env_vars[i].key);
    free(env_vars[i].value);
  }
  free(env_vars);
}

EnvVars get_env_vars_with_count() {
  maybe_make_export_env();
  char **env = export_env;

  int count = 0;
  while (env[count])
    count++;

  CEnv *env_vars = (CEnv *)malloc(sizeof(CEnv) * count);
  if (!env_vars) {
    perror("Failed to allocate memory for environment variables");
    return (EnvVars){NULL, 0}; // Return an empty structure with count zero
  }

  int i = 0;
  while (*env) {
    char *delimiter = strchr(*env, '=');
    if (!delimiter) {
      fprintf(stderr, "Environment variable '%s' is malformed\n", *env);
      continue;
    }

    int key_len = delimiter - *env;
    int value_len = strlen(delimiter + 1);

    env_vars[i].key = malloc(key_len + 1);
    env_vars[i].value = malloc(value_len + 1);

    if (!env_vars[i].key || !env_vars[i].value) {
      perror("Failed to allocate memory for key/value");
      for (int j = 0; j < i; j++) {
        free(env_vars[j].key);
        free(env_vars[j].value);
      }
      free(env_vars);
      return (EnvVars){NULL, 0};
    }

    strncpy(env_vars[i].key, *env, key_len);
    env_vars[i].key[key_len] = '\0';
    strcpy(env_vars[i].value, delimiter + 1);

    i++;
    env++;
  }

  return (EnvVars){env_vars, i};
}
