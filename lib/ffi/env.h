#ifndef __FFI_ENV_H__
#define __FFI_ENV_H__

#include "builtins.h"
#include "variables.h"
#include <arrayfunc.h>

typedef struct CEnv {
  char *key;
  char *value;
} CEnv;

typedef struct {
  CEnv *env_vars;
  int count;
} EnvVars;

extern char **export_env;

EnvVars get_env_vars_with_count();
void free_env_vars(CEnv *env_vars, int count);

#endif /* __FFI_ENV_H__ */
