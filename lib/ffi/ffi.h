#ifndef __FFI_H__
#define __FFI_H__

#include <builtins.h>
#include <command.h>
#include <variables.h>

typedef struct word_arr {
  char **array;
  int count;
} WORD_ARR;

void init_dynamic_var(char *varname, char *val,
                      SHELL_VAR *(*get_func)(SHELL_VAR *),
                      SHELL_VAR *(*assign_func)(SHELL_VAR *, char *, long,
                                                char *));
WORD_ARR convert_word_list(WORD_LIST *list);
extern int execute_shell_function PARAMS((SHELL_VAR *, WORD_LIST *));
#endif /* __FFI_H__ */
