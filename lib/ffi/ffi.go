package ffi

/*
#cgo pkg-config: bash
#cgo linux LDFLAGS: -Wl,-unresolved-symbols=ignore-all
#cgo darwin LDFLAGS: -Wl,-undefined,dynamic_lookup

#include  "ffi.h"

*/
import "C"

import (
	"fmt"
	"unsafe"
)

type (
	WORD_LIST         C.WORD_LIST
	WORD_LIST_PTR     *C.WORD_LIST
	WORD_LIST_PTR_PTR **C.WORD_LIST

	WORD_DESC         C.WORD_DESC
	WORD_DESC_PTR     *C.WORD_DESC
	WORD_DESC_PTR_PTR **C.WORD_DESC

	SHELL_VAR         C.SHELL_VAR
	SHELL_VAR_PTR     *C.SHELL_VAR
	SHELL_VAR_PTR_PTR **C.SHELL_VAR
)

func NewWordDesc(cstr *C.char) *C.WORD_DESC {
	wd := C.malloc(C.sizeof_WORD_DESC)
	if wd == nil {
		return nil
	}
	wordDesc := (*C.WORD_DESC)(wd)
	wordDesc.word = C.CString(C.GoString(cstr))
	return wordDesc
}

func NewWordList(cstr *C.char) *C.WORD_LIST {
	wl := C.malloc(C.sizeof_WORD_LIST)
	if wl == nil {
		return nil
	}

	wordList := (*C.WORD_LIST)(wl)
	wordList.word = NewWordDesc(cstr)
	wordList.next = nil

	return wordList
}

func AppendWordList(head **C.WORD_LIST, cstr *C.char) {
	newNode := NewWordList(cstr)
	if *head == nil {
		*head = newNode
	} else {
		current := *head
		for current.next != nil {
			current = current.next
		}
		current.next = newNode
	}
}

func FreeWordDesc(wd WORD_DESC_PTR) {
	if wd != nil {
		C.free(unsafe.Pointer(wd.word))
		C.free(unsafe.Pointer(wd))
	}
}

func FreeWordList(head WORD_LIST_PTR) {
	var next *C.WORD_LIST
	for head != nil {
		next = head.next
		FreeWordDesc(head.word)
		C.free(unsafe.Pointer(head))
		head = next
	}
}

func ConvertWordList(ptr WORD_LIST_PTR) []string {
	argv := []string{}
	args := (*C.WORD_LIST)(unsafe.Pointer(ptr))
	for args != nil {
		argv = append(argv, C.GoString(args.word.word))
		args = args.next
	}
	return argv
}

func ExecuteShellFunction(func_name string, args unsafe.Pointer) error {
	cfunc_name := C.CString(func_name)
	shell_var := C.find_function(cfunc_name)

	// defer C.free(unsafe.Pointer(cfunc_name))

	if shell_var == nil {
		return fmt.Errorf("failed to find shell function %s", func_name)
	}

	words_list := NewWordList(cfunc_name)

	// shell_vars := (*C.SHELL_VAR)(args)
	// AppendWordList(&words_list, shell_vars.name)
	// if shell_vars.value != nil {
	// 	AppendWordList(&words_list, shell_vars.value)
	// }

	if shell_var != nil {
		C.execute_shell_function(shell_var, words_list)
	}

	FreeWordList(words_list)

	return nil
}

func InitDynVar(name string, value *C.char, get_func, assign_func *[0]byte) {
	cname := C.CString(name)
	C.init_dynamic_var(cname, value, get_func, assign_func)
	C.free(unsafe.Pointer(cname))
}
