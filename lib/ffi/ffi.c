#include "ffi.h"
#include <stdio.h>

extern int execute_shell_function PARAMS((SHELL_VAR *, WORD_LIST *));

void init_dynamic_var(char *varname, char *val,
                      SHELL_VAR *(*get_func)(SHELL_VAR *),
                      SHELL_VAR *(*assign_func)(SHELL_VAR *, char *, long,
                                                char *)) {

  // SHELL_VAR *var = bind_variable(varname, (val), 0);
  SHELL_VAR *var = bind_global_variable(varname, (val), 0);
  var->dynamic_value = get_func;
  var->assign_func = assign_func;
}

WORD_ARR convert_word_list(WORD_LIST *list) {
  int count = 0;
  WORD_LIST *l;
  for (l = list; l; l = l->next) {
    count++;
  }

  char **words = (char **)malloc(count * sizeof(char *));
  if (!words)
    return (WORD_ARR){NULL, 0};

  int i = 0;
  for (l = list; l; l = l->next) {
    words[i++] = l->word->word;
  }

  free(l);

  return (WORD_ARR){words, i};
}
