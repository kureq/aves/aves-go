package ffi

/*
#cgo pkg-config: bash
#cgo linux LDFLAGS: -Wl,-unresolved-symbols=ignore-all
#cgo darwin LDFLAGS: -Wl,-undefined,dynamic_lookup

#include  "env.h"
*/
import "C"

import (
	"fmt"
	"unsafe"
)

func Get(key string) (string, error) {
	cKey := C.CString(key)
	defer C.free(unsafe.Pointer(cKey))

	cEnv := C.find_variable(cKey)
	if cEnv == nil {
		return "", fmt.Errorf("environment variable %s not found", key)
	}

	return C.GoString(cEnv.value), nil
}

func GetShell(key string) (string, error) {
	cKey := C.CString(key)
	defer C.free(unsafe.Pointer(cKey))

	cEnv := C.find_shell_variable(cKey)
	if cEnv == nil {
		return "", fmt.Errorf("environment variable %s not found", key)
	}

	return C.GoString(cEnv.value), nil
}

func Set(key, value string) {
	cKey := C.CString(key)
	defer C.free(unsafe.Pointer(cKey))

	cValue := C.CString(value)
	defer C.free(unsafe.Pointer(cValue))

	C.bind_variable(cKey, cValue, C.int(0))
}

func SetInternal(key, value string) {
	cKey := C.CString(key)
	defer C.free(unsafe.Pointer(cKey))

	cValue := C.CString(value)
	defer C.free(unsafe.Pointer(cValue))

	C.bind_variable(cKey, cValue, C.int(1))
}

func SetArray(key string, values []string) {
	cKey := C.CString(key)
	defer C.free(unsafe.Pointer(cKey))

	/*If FLAGS&1 is non-zero, an existing
		  variable is checked for the readonly or noassign attribute in preparation
		  for assignment (e.g., by the `read' builtin).  If FLAGS&2 is non-zero,
	we create an associative array. */
	arr := C.find_or_make_array_variable(cKey, C.int(1))
	flag := C.int(0)

	for i, value := range values {
		cValue := C.CString(value)
		cIndex := C.long(i)
		defer C.free(unsafe.Pointer(cValue))

		/*
		   Parse NAME, a lhs of an assignment statement of the form v[s], and
		   	assign VALUE to that array element by calling bind_array_variable().
		   	Flags are ASS_ assignment flags
		*/
		C.bind_array_element(arr, cIndex, cValue, flag)
	}
}

func SetLocal(key, value string) {
	cKey := C.CString(key)
	defer C.free(unsafe.Pointer(cKey))

	cValue := C.CString(value)
	defer C.free(unsafe.Pointer(cValue))

	C.bind_variable(cKey, cValue, 1)
	C.make_local_variable(cKey, 1)
}

func Unset(key string) {
	cKey := C.CString(key)
	defer C.free(unsafe.Pointer(cKey))

	C.unbind_variable(cKey)
}

func List() []string {
	cEnvVars := C.get_env_vars_with_count()
	defer C.free_env_vars(cEnvVars.env_vars, cEnvVars.count)

	if cEnvVars.env_vars == nil || cEnvVars.count == 0 {
		fmt.Println("No environment variables found or memory allocation failed.")
		return nil
	}

	goEnvVars := make([]string, cEnvVars.count)

	envVarsPtr := uintptr(unsafe.Pointer(cEnvVars.env_vars))
	sizeOfCEnv := unsafe.Sizeof(*cEnvVars.env_vars)

	for i := 0; i < int(cEnvVars.count); i++ {
		envVar := (*C.CEnv)(unsafe.Pointer(envVarsPtr + uintptr(i)*sizeOfCEnv))
		goEnvVars[i] = C.GoString(envVar.key) + "=" + C.GoString(envVar.value)
	}

	return goEnvVars
}

func Map() map[string]string {
	cEnvVars := C.get_env_vars_with_count()
	defer C.free_env_vars(cEnvVars.env_vars, cEnvVars.count)

	if cEnvVars.env_vars == nil || cEnvVars.count == 0 {
		fmt.Println("No environment variables found or memory allocation failed.")
		return nil
	}

	goEnvVars := make(map[string]string, cEnvVars.count)

	envVarsPtr := uintptr(unsafe.Pointer(cEnvVars.env_vars))
	sizeOfCEnv := unsafe.Sizeof(*cEnvVars.env_vars)

	for i := 0; i < int(cEnvVars.count); i++ {
		envVar := (*C.CEnv)(unsafe.Pointer(envVarsPtr + uintptr(i)*sizeOfCEnv))
		goEnvVars[C.GoString(envVar.key)] = C.GoString(envVar.value)
	}

	return goEnvVars
}
