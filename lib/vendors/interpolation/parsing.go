package interpolation

// Rewrite of https://github.com/buildkite/interpolate/tree/main, License: MIT
//# Buildkite Licence

// Copyright (c) 2014-2017 Buildkite Pty Ltd
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	eof = -1
)

// Parser takes a string and parses out a tree of structs that represent text and Expansions
type Parser struct {
	input string // the string we are scanning
	pos   int    // the current position
}

// NewParser returns a new instance of a Parser
func NewParser(str string) *Parser {
	return &Parser{
		input: str,
		pos:   0,
	}
}

// Parse expansions out of the internal text and return them as a tree of Expressions
func (p *Parser) Parse() (Expression, error) {
	return p.parseExpression()
}

func (p *Parser) parseExpression(stop ...rune) (Expression, error) {
	var expr Expression
	stopStr := string(stop)

	for {
		c := p.peekRune()
		if c == eof || strings.ContainsRune(stopStr, c) {
			break
		}

		// check for our escaped characters first, as we assume nothing subsequently is escaped
		if strings.HasPrefix(p.input[p.pos:], `\\`) {
			p.pos += 2
			expr = append(expr, ExpressionItem{Text: `\\`})
			continue
		}

		if strings.HasPrefix(p.input[p.pos:], `\$`) || strings.HasPrefix(p.input[p.pos:], `$$`) {
			p.pos += 2

			ee, err := p.parseEscapedExpansion()
			if err != nil {
				return nil, err
			}

			expr = append(expr, ExpressionItem{Expansion: ee})
			continue
		}

		// Ignore bash shell expansions
		if strings.HasPrefix(p.input[p.pos:], `$(`) {
			p.pos += 2
			expr = append(expr, ExpressionItem{Text: `$(`})
			continue
		}

		// If we run into a dollar sign and it's not the last char, it's an expansion
		if c == '$' && p.pos < (len(p.input)-1) {
			expansion, err := p.parseExpansion()
			if err != nil {
				return nil, err
			}
			expr = append(expr, ExpressionItem{Expansion: expansion})
			continue
		}

		// nibble a character, otherwise if it's a \ or a $ we can loop
		c = p.nextRune()

		// Scan as much as we can into text
		text := p.scanUntil(func(r rune) bool {
			return (r == '$' || r == '\\' || r == '%' || strings.ContainsRune(stopStr, r))
		})

		expr = append(expr, ExpressionItem{Text: string(c) + text})
	}

	return expr, nil
}

func (p *Parser) parseEscapedExpansion() (Expansion, error) {
	next := p.peekRune()
	switch {
	case next == '{':
		// if it's an escaped brace expansion, (eg $${MY_COOL_VAR:-5}) consume text until the close brace
		id := p.scanUntil(func(r rune) bool { return r == '}' })
		id = id + string(p.nextRune()) // we know that the next rune is a close brace, chuck it on the end
		return EscapedExpansion{Identifier: id}, nil

	case unicode.IsLetter(next):
		// it's an escaped identifier (eg $$MY_COOL_VAR)
		id, err := p.scanIdentifier()
		if err != nil {
			return nil, err
		}

		return EscapedExpansion{Identifier: id}, nil

	default:
		// there's no identifier or brace afterward, so it's probably a literal escaped dollar sign, so return an empty identifier
		// that will be expanded to a single dollar sign
		return EscapedExpansion{Identifier: ""}, nil
	}
}

func (p *Parser) parseExpansion() (Expansion, error) {
	if c := p.nextRune(); c != '$' {
		return nil, fmt.Errorf("expected expansion to start with $, got %c", c)
	}

	// if we have an open brace, this is a brace expansion
	if c := p.peekRune(); c == '{' {
		return p.parseBraceExpansion()
	}

	identifier, err := p.scanIdentifier()
	if err != nil {
		return nil, err
	}

	return VariableExpansion{Identifier: identifier}, nil
}

func (p *Parser) parseBraceExpansion() (Expansion, error) {
	if c := p.nextRune(); c != '{' {
		return nil, fmt.Errorf("expected brace expansion to start with {, got %c", c)
	}

	identifier, err := p.scanIdentifier()
	if err != nil {
		return nil, err
	}

	if c := p.peekRune(); c == '}' {
		_ = p.nextRune()
		return VariableExpansion{Identifier: identifier}, nil
	}

	var operator string
	var exp Expansion

	// Parse an operator, some trickery is needed to handle : vs :-
	if op1 := p.nextRune(); op1 == ':' {
		if op2 := p.peekRune(); op2 == '-' {
			_ = p.nextRune()
			operator = ":-"
		} else {
			operator = ":"
		}
	} else if op1 == '?' || op1 == '-' {
		operator = string(op1)
	} else {
		return nil, fmt.Errorf("expected an operator, got %c", op1)
	}

	switch operator {
	case `:-`:
		exp, err = p.parseEmptyValueExpansion(identifier)
		if err != nil {
			return nil, err
		}
	case `-`:
		exp, err = p.parseUnsetValueExpansion(identifier)
		if err != nil {
			return nil, err
		}
	case `:`:
		exp, err = p.parseSubstringExpansion(identifier)
		if err != nil {
			return nil, err
		}
	case `?`:
		exp, err = p.parseRequiredExpansion(identifier)
		if err != nil {
			return nil, err
		}
	}

	if c := p.nextRune(); c != '}' {
		return nil, fmt.Errorf("expected brace expansion to end with }, got %c", c)
	}

	return exp, nil
}

func (p *Parser) parseEmptyValueExpansion(identifier string) (Expansion, error) {
	// parse an expression (text and expansions) up until the end of the brace
	expr, err := p.parseExpression('}')
	if err != nil {
		return nil, err
	}

	return EmptyValueExpansion{Identifier: identifier, Content: expr}, nil
}

func (p *Parser) parseUnsetValueExpansion(identifier string) (Expansion, error) {
	expr, err := p.parseExpression('}')
	if err != nil {
		return nil, err
	}

	return UnsetValueExpansion{Identifier: identifier, Content: expr}, nil
}

func (p *Parser) parseSubstringExpansion(identifier string) (Expansion, error) {
	offset := p.scanUntil(func(r rune) bool {
		return r == ':' || r == '}'
	})

	offsetInt, err := strconv.Atoi(strings.TrimSpace(offset))
	if err != nil {
		return nil, fmt.Errorf("unable to parse offset: %v", err)
	}

	if c := p.peekRune(); c == '}' {
		return SubstringExpansion{Identifier: identifier, Offset: offsetInt}, nil
	}

	_ = p.nextRune()
	length := p.scanUntil(func(r rune) bool {
		return r == '}'
	})

	lengthInt, err := strconv.Atoi(strings.TrimSpace(length))
	if err != nil {
		return nil, fmt.Errorf("unable to parse length: %v", err)
	}

	return SubstringExpansion{Identifier: identifier, Offset: offsetInt, Length: lengthInt, HasLength: true}, nil
}

func (p *Parser) parseRequiredExpansion(identifier string) (Expansion, error) {
	expr, err := p.parseExpression('}')
	if err != nil {
		return nil, err
	}

	return RequiredExpansion{Identifier: identifier, Message: expr}, nil
}

func (p *Parser) scanUntil(f func(rune) bool) string {
	start := p.pos
	for int(p.pos) < len(p.input) {
		c, size := utf8.DecodeRuneInString(p.input[p.pos:])
		if c == utf8.RuneError || f(c) {
			break
		}
		p.pos += size
	}
	return p.input[start:p.pos]
}

func (p *Parser) scanIdentifier() (string, error) {
	if c := p.peekRune(); !unicode.IsLetter(c) {
		if c == '.' {
			// This is a special case for the config file. We allow dots in the identifier
			// so that we can have nested objects in the config file. This is not standard
			// bash behavior, but it's useful for us.
			return p.scanUntil(func(r rune) bool {
				return (!unicode.IsLetter(r) && !unicode.IsNumber(r) && r != '_' && r != '.')
			}), nil
		}
		return "", fmt.Errorf("expected identifier to start with a letter, got %c", c)
	}
	notIdentifierChar := func(r rune) bool {
		return (!unicode.IsLetter(r) && !unicode.IsNumber(r) && r != '_')
	}
	return p.scanUntil(notIdentifierChar), nil
}

func (p *Parser) nextRune() rune {
	if int(p.pos) >= len(p.input) {
		return eof
	}
	c, size := utf8.DecodeRuneInString(p.input[p.pos:])
	p.pos += size
	return c
}

func (p *Parser) peekRune() rune {
	if int(p.pos) >= len(p.input) {
		return eof
	}
	c, _ := utf8.DecodeRuneInString(p.input[p.pos:])
	return c
}
