package interpolation

// Rewrite of https://github.com/buildkite/interpolate/tree/main, License: MIT
//# Buildkite Licence

// Copyright (c) 2014-2017 Buildkite Pty Ltd
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import (
	"bytes"
	"fmt"
	"strings"

	"gitlab.com/kureq/aves/aves-go/lib/ffi"
)

type getter func(string) string

// Interpolate takes a set of environment and interpolates it into the provided string using shell script expansions
func Interpolate(str string, get getter) (string, error) {
	expr, err := NewParser(str).Parse()
	if err != nil {
		return "", err
	}
	return expr.Expand(get)
}

// Indentifiers parses the identifiers from any expansions in the provided string
func Identifiers(str string, get getter) ([]string, error) {
	expr, err := NewParser(str).Parse()
	if err != nil {
		return nil, err
	}
	return expr.Identifiers(), nil
}

func GetVar(key string, get getter) (string, error) {
	if strings.HasPrefix(key, ".") && get != nil {
		key = strings.TrimPrefix(key, ".")
		val := get(key)

		if val == "" {
			return "", fmt.Errorf("environment variable %s not found", key)
		}

		return val, nil
	} else {
		val, err := ffi.Get(key)
		return val, err
	}
}

// An expansion is something that takes in ENV and returns a string or an error
type Expansion interface {
	Expand(get getter) (string, error)
	Identifiers() []string
}

// VariableExpansion represents either $VAR or ${VAR}, our simplest expansion
type VariableExpansion struct {
	Identifier string
}

func (e VariableExpansion) Identifiers() []string {
	return []string{e.Identifier}
}

func (e VariableExpansion) Expand(get getter) (string, error) {
	val, _ := GetVar(e.Identifier, get)
	return val, nil
}

// EmptyValueExpansion returns either the value of an env, or a default value if it's unset or null
type EmptyValueExpansion struct {
	Identifier string
	Content    Expression
}

func (e EmptyValueExpansion) Identifiers() []string {
	return append([]string{e.Identifier}, e.Content.Identifiers()...)
}

func (e EmptyValueExpansion) Expand(get getter) (string, error) {
	val, _ := GetVar(e.Identifier, get)
	if val == "" {
		return e.Content.Expand(get)
	}
	return val, nil
}

// UnsetValueExpansion returns either the value of an env, or a default value if it's unset
type UnsetValueExpansion struct {
	Identifier string
	Content    Expression
}

func (e UnsetValueExpansion) Identifiers() []string {
	return []string{e.Identifier}
}

func (e UnsetValueExpansion) Expand(get getter) (string, error) {
	val, err := GetVar(e.Identifier, get)
	if err == nil {
		return e.Content.Expand(get)
	}
	return val, nil
}

// EscapedExpansion is an expansion that is delayed until later on (usually by a later process)
type EscapedExpansion struct {
	Identifier string
}

func (e EscapedExpansion) Identifiers() []string {
	return []string{"$" + e.Identifier}
}

func (e EscapedExpansion) Expand(_ getter) (string, error) {
	return "$" + e.Identifier, nil
}

// SubstringExpansion returns a substring (or slice) of the env
type SubstringExpansion struct {
	Identifier string
	Offset     int
	Length     int
	HasLength  bool
}

func (e SubstringExpansion) Identifiers() []string {
	return []string{e.Identifier}
}

func (e SubstringExpansion) Expand(get getter) (string, error) {
	val, _ := GetVar(e.Identifier, get)

	from := e.Offset

	// Negative offsets = from end
	if from < 0 {
		from += len(val)
	}

	// Still negative = too far from end? Truncate to start.
	if from < 0 {
		from = 0
	}

	// Beyond end? Truncate to end.
	if from > len(val) {
		from = len(val)
	}

	if !e.HasLength {
		return val[from:], nil
	}

	to := e.Length

	if to >= 0 {
		// Positive length = from offset
		to += from
	} else {
		// Negative length = from end
		to += len(val)

		// Too far? Truncate to offset.
		if to < from {
			to = from
		}
	}

	// Beyond end? Truncate to end.
	if to > len(val) {
		to = len(val)
	}

	return val[from:to], nil
}

// RequiredExpansion returns an env value, or an error if it is unset
type RequiredExpansion struct {
	Identifier string
	Message    Expression
}

func (e RequiredExpansion) Identifiers() []string {
	return []string{e.Identifier}
}

func (e RequiredExpansion) Expand(get getter) (string, error) {
	val, err := GetVar(e.Identifier, get)
	if err != nil {
		msg, err := e.Message.Expand(get)
		if err != nil {
			return "", err
		}
		if msg == "" {
			msg = "not set"
		}
		return "", fmt.Errorf("$%s: %s", e.Identifier, msg)
	}
	return val, nil
}

// Expression is a collection of either Text or Expansions
type Expression []ExpressionItem

func (e Expression) Identifiers() []string {
	identifiers := []string{}
	for _, item := range e {
		if item.Expansion != nil {
			identifiers = append(identifiers, item.Expansion.Identifiers()...)
		}
	}
	return identifiers
}

func (e Expression) Expand(get getter) (string, error) {
	buf := &bytes.Buffer{}

	for _, item := range e {
		if item.Expansion != nil {
			result, err := item.Expansion.Expand(get)
			if err != nil {
				return "", err
			}
			_, _ = buf.WriteString(result)
		} else {
			_, _ = buf.WriteString(item.Text)
		}
	}

	return buf.String(), nil
}

// ExpressionItem models either an Expansion or Text. Either/Or, never both.
type ExpressionItem struct {
	Expansion Expansion
	Text      string
}

func (i ExpressionItem) String() string {
	if i.Expansion != nil {
		return fmt.Sprintf("%#v", i.Expansion)
	}
	return fmt.Sprintf("%q", i.Text)
}
