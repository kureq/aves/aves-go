package yaml

import (
	"github.com/go-faster/yaml"
	// "gopkg.in/yaml.v3"
)

// YAML implements a YAML parser.
type YAML struct{}

func New() *YAML {
	return &YAML{}
}

func (p *YAML) Unmarshal(b []byte) (map[string]interface{}, error) {
	var out map[string]interface{}
	if err := yaml.Unmarshal(b, &out); err != nil {
		return nil, err
	}

	return out, nil
}

func (p *YAML) Marshal(o interface{}) ([]byte, error) {
	return yaml.Marshal(o)
}
