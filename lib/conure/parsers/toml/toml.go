package toml

import "github.com/pelletier/go-toml/v2"

type TOML struct{}

func New() *TOML {
	return &TOML{}
}

func (p *TOML) Unmarshal(b []byte) (map[string]interface{}, error) {
	var out map[string]interface{}
	if err := toml.Unmarshal(b, &out); err != nil {
		return nil, err
	}

	return out, nil
}

func (p *TOML) Marshal(o interface{}) ([]byte, error) {
	return toml.Marshal(o)
}
