package json

import "encoding/json"

type JSON struct{}

func New() *JSON {
	return &JSON{}
}

func (p *JSON) Unmarshal(b []byte) (map[string]interface{}, error) {
	var out map[string]interface{}
	if err := json.Unmarshal(b, &out); err != nil {
		return nil, err
	}

	return out, nil
}

func (p *JSON) Marshal(o interface{}) ([]byte, error) {
	return json.Marshal(o)
}
