package conure

import (
	"strings"

	"gitlab.com/kureq/aves/aves-go/lib/conure/merge"
	"gitlab.com/kureq/aves/aves-go/lib/conure/providers/file"
)

type Conure struct {
	Parser    Parser
	Provider  Provider
	Interface map[string]interface{}
}

type Provider interface {
	ReadBytes() ([]byte, error)
	Read() (map[string]interface{}, error)
}

func New() *Conure {
	return &Conure{}
}

func (c *Conure) LoadFile(path string, pars Parser, options merge.Config) (err error) {
	if err = c.Load(file.Provider(path), pars, options); err != nil {
		return err
	}

	return nil
}

func (c *Conure) Load(prov Provider, pars Parser, options merge.Config) (err error) {
	c.Provider = prov
	c.Parser = pars

	var (
		bytes []byte
		intf  map[string]interface{}
	)

	bytes, err = c.Provider.ReadBytes()
	if err != nil {
		return err
	}

	intf, err = c.Parser.Unmarshal(bytes)
	if err != nil {
		return err
	}

	if c.Interface == nil {
		c.Interface = intf
		return nil
	}

	c.merge(intf, options)

	return nil
}

func (c *Conure) Set(key string, val interface{}) (interface{}, error) {
	n := Unflatten(map[string]interface{}{key: val})

	return c.merge(n, merge.Config{Strict: false})
}

func (c *Conure) Merge(new map[string]interface{}, confg ...merge.Config) (map[string]interface{}, error) {
	return c.merge(new, confg...)
}

func (c *Conure) merge(new map[string]interface{}, confg ...merge.Config) (map[string]interface{}, error) {
	var config merge.Config

	if len(confg) == 0 {
		config = merge.Config{
			Strict:       false,
			AppendArrays: true,
		}
	} else {
		config = confg[0]
	}

	err := merge.Merge(c.Interface, new, config)
	if err != nil {
		return nil, err
	}

	return c.Interface, nil
}

func Unflatten(m map[string]interface{}) map[string]interface{} {
	out := make(map[string]interface{})

	for k, v := range m {
		var (
			keys []string
			next = out
		)

		keys = strings.Split(k, ".")

		for _, k := range keys[:len(keys)-1] {
			sub, ok := next[k]
			if !ok {
				sub = make(map[string]interface{})
				next[k] = sub
			}
			if n, ok := sub.(map[string]interface{}); ok {
				next = n
			}
		}

		next[keys[len(keys)-1]] = v
	}
	return out
}
