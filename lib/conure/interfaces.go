package conure

type Parser interface {
	Unmarshal([]byte) (map[string]interface{}, error)
	Marshal(interface{}) ([]byte, error)
}
