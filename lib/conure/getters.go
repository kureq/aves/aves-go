package conure

import (
	"fmt"
	"strings"

	"gitlab.com/kureq/aves/aves-go/lib/vendors/interpolation"
	"gitlab.com/kureq/aves/aves-go/lib/vendors/jp"
)

type GetOptions struct {
	Parser Parser
	Split  bool
	Array  bool
	Group  bool
}

func (c *Conure) Get(query string) interface{} {
	var results interface{}
	var err error

	exp, err := jp.ParseString(query)
	if err != nil {
		return nil
	}

	results = exp.Get(c.Interface)

	return results
}

func (c *Conure) String(query string) string {
	if v := c.Get(query); v != nil {
		if i, ok := v.(string); ok {
			j, err := interpolation.Interpolate(i, c.String)
			if err != nil {
				return ""
			}
			return j
		}
		return fmt.Sprintf("%v", v)
	}
	return ""
}

func (c *Conure) Strings(query string, options GetOptions) []string {
	v := c.Get(query)
	if v == nil {
		return []string{}
	}

	if options.Parser == nil {
		options.Parser = c.Parser
	}

	switch t := v.(type) {
	case []interface{}:
		out := make([]string, 0, len(t))
		for _, v := range t {
			switch v := v.(type) {
			case nil:
				out = append(out, "") // TODO(VBE): Should maybe indicate NULL on unset
			case string:
				interpolated, err := interpolation.Interpolate(v, c.String)
				if err != nil {
					return []string{}
				}
				out = append(out, interpolated)
			case map[string]interface{}:
				if options.Split {
					for key, val := range v {
						out = append(out, key)
						switch vt := val.(type) {
						case string:
							interpolated, err := interpolation.Interpolate(vt, c.String)
							if err != nil {
								return []string{}
							}
							out = append(out, interpolated)
						default:
							// Just using the default value for marshalling
							val, err := options.Parser.Marshal(val)
							if err != nil {
								return []string{}
							}
							mv := strings.TrimSuffix(string(val), "\n")
							out = append(out, string(mv))
						}

					}
				} else if options.Array {
					if m, err := options.Parser.Marshal(v); err == nil {
						out = append(out, string(m))
					} else {
						return []string{}
					}
				} else {
					for key, val := range v {
						if m, err := options.Parser.Marshal(map[string]interface{}{key: val}); err == nil {
							interpolated, err := interpolation.Interpolate(string(m), c.String)
							if err != nil {
								return []string{}
							}
							out = append(out, string(interpolated))
						} else {
							return []string{}
						}
					}
				}
			default:
				m, err := options.Parser.Marshal(v)
				if err != nil {
					return []string{}
				}
				// HACK(VBE): Removing trailing new line (m = m[:len(m)-1])
				mv := strings.TrimSuffix(string(m), "\n")
				out = append(out, string(mv))
			}
		}
		return out

	case []string:
		out := make([]string, len(t))
		copy(out, t)
		return out

	case map[string]interface{}:
		if options.Split {
			out := make([]string, 0, len(t))
			for k, v := range t {
				out = append(out, k)
				val, err := options.Parser.Marshal(v)
				if err != nil {
					return []string{}
				}
				out = append(out, string(val))
			}
			return out
		} else {
			out := make([]string, 0, len(t))
			for k, v := range t {
				if m, err := options.Parser.Marshal(map[string]interface{}{k: v}); err == nil {
					out = append(out, string(m))
				} else {
					return []string{}
				}
			}
			return out
		}

	case string:
		if interpolated, err := interpolation.Interpolate(t, c.String); err == nil {
			return []string{interpolated}
		} else {
			return []string{}
		}

	}
	return []string{}
}
