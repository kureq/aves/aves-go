package raw

type Raw struct {
	b []byte
}

func Provider(b []byte) *Raw {
	r := &Raw{b: make([]byte, (len(b)))}
	copy(r.b[:], b)
	return r
}

func (r *Raw) ReadBytes() ([]byte, error) {
	return r.b, nil
}

func (r *Raw) Read() (map[string]interface{}, error) {
	return nil, nil
}
