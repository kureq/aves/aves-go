package merge

import (
	"fmt"
)

type Config struct {
	Strict       bool
	AppendArrays bool
}

func Merge(old, new map[string]interface{}, config Config) error {
	for nkey, nval := range new {
		if nmap, ok := nval.(map[string]interface{}); ok {
			if oval, ok := old[nkey]; ok {
				if omap, ok := oval.(map[string]interface{}); ok {
					err := Merge(omap, nmap, config)
					if err != nil {
						return err
					}
					continue
				}
			} else {
				old[nkey] = make(map[string]interface{})
			}

			switch old[nkey].(type) {
			case map[string]interface{}:
				err := Merge(old[nkey].(map[string]interface{}), nmap, config)
				if err != nil {
					return err
				}
			default:
				// If the key is not a map, then we can't merge it, so we just replace it
				old[nkey] = nmap
			}

		} else if nslice, ok := nval.([]interface{}); ok {
			if !config.AppendArrays {
				old[nkey] = nslice
				continue
			} else if oval, ok := old[nkey]; ok {
				if oslice, ok := oval.([]interface{}); ok {
					old[nkey] = append(oslice, nslice...)
					continue
				}
			}
			old[nkey] = nslice
		} else {
			if config.Strict {
				if _, ok := old[nkey]; ok {
					return fmt.Errorf("key %s already exists", nkey)
				}
			}
			old[nkey] = nval
		}
	}
	return nil
}
