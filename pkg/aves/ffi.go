package aves

/*
#cgo pkg-config: bash
#cgo linux LDFLAGS: -Wl,-unresolved-symbols=ignore-all
#cgo darwin LDFLAGS: -Wl,-undefined,dynamic_lookup

#include "ffi.h"
*/
import "C"

import (
	"fmt"
	"unsafe"

	"github.com/jessevdk/go-flags"
)

// PERF(VBE): This has to be implemented in all builtin packages...
func ConvertWordList(args *C.WORD_LIST) []string {
	argv := []string{}
	for args != nil {
		argv = append(argv, C.GoString(args.word.word))
		args = args.next
	}
	return argv
}

func NewWordDesc(cstr *C.char) *C.WORD_DESC {
	wd := C.malloc(C.sizeof_WORD_DESC)
	if wd == nil {
		return nil
	}
	wordDesc := (*C.WORD_DESC)(wd)
	wordDesc.word = C.CString(C.GoString(cstr))
	return wordDesc
}

func NewWordList(cstr *C.char) *C.WORD_LIST {
	wl := C.malloc(C.sizeof_WORD_LIST)
	if wl == nil {
		return nil
	}

	wordList := (*C.WORD_LIST)(wl)
	wordList.word = NewWordDesc(cstr)
	wordList.next = nil
	return wordList
}

func AppendWordList(head **C.WORD_LIST, cstr *C.char) {
	newNode := NewWordList(cstr)
	if *head == nil {
		*head = newNode
	} else {
		current := *head
		for current.next != nil {
			current = current.next
		}
		current.next = newNode
	}
}

func FreeWordDesc(wd *C.WORD_DESC) {
	if wd != nil {
		C.free(unsafe.Pointer(wd.word))
		C.free(unsafe.Pointer(wd))
	}
}

func FreeWordList(head *C.WORD_LIST) {
	var next *C.WORD_LIST
	for head != nil {
		next = head.next
		FreeWordDesc(head.word)
		C.free(unsafe.Pointer(head))
		head = next
	}
}

//export aves_builtin
func aves_builtin(args *C.WORD_LIST) C.int {
	flock = AvesCLI{}

	if _, err := aves_parser.ParseArgs(ConvertWordList(args)); err != nil {
		switch flagsErr := err.(type) {
		case flags.ErrorType:
			if flagsErr == flags.ErrHelp {
				return C.int(0)
			}
			return C.int(1)
		default:
			fmt.Println(err)
			return C.int(1)
		}
	}

	return C.int(0)
}

//export aves_builtin_load
func aves_builtin_load(_ *C.char) C.int {
	aves_parser.SubcommandsOptional = false
	aves_parser.Name = "aves"

	return C.int(1)
}

//export aves_builtin_unload
func aves_builtin_unload(_ *C.char) C.int {
	return 0
}
