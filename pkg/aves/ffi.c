#include "ffi.h"

void create_dynamic_var(char *varname, char *val,
                        SHELL_VAR *(*get_func)(SHELL_VAR *),
                        SHELL_VAR *(*assign_func)(SHELL_VAR *, char *, long,
                                                  char *)) {
  SHELL_VAR *var = bind_variable(varname, (val), 0);
  var->dynamic_value = get_func;
  var->assign_func = assign_func;
}
