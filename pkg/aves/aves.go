package aves

import "github.com/jessevdk/go-flags"

var (
	flock       AvesCLI
	aves_parser = flags.NewParser(&flock, flags.HelpFlag|flags.PassDoubleDash)
)

type AvesCLI struct {
	Dynamic DynamicVarCMD `command:"dynamic" description:"Manage dynamic variables"`
}
