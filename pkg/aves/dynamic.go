package aves

/*
#cgo pkg-config: bash
#cgo linux LDFLAGS: -Wl,-unresolved-symbols=ignore-all
#cgo darwin LDFLAGS: -Wl,-undefined,dynamic_lookup

#include "ffi.h"
#include <builtins.h>
#include <variables.h>

extern int execute_shell_function PARAMS((SHELL_VAR *, WORD_LIST *));
extern SHELL_VAR *dynamicvar_assign_wrapper(SHELL_VAR *, char *, int, char *);
extern SHELL_VAR *dynamicvar_get_wrapper(SHELL_VAR *);
*/
import "C"

import (
	"fmt"
	"unsafe"
)

type DynamicVars struct {
	variable string
	function string
}

var dynamicVars = make(map[string]*DynamicVars)

// FFI functions

// Some day I will find a way to disable the unused warnings
//
//export dynamicvar_assign
func dynamicvar_assign(self *C.SHELL_VAR, value *C.char, unused C.int, _ *C.char) *C.SHELL_VAR {
	self.value = value
	return self
}

// This will be called from the shell, and will call a shell function to get the value
//
//export dynamicvar_get
func dynamicvar_get(self *C.SHELL_VAR) *C.SHELL_VAR {
	name := C.GoString(self.name)

	if dynamicVars[name] != nil {
		func_name := dynamicVars[name].function

		func_getter := C.CString(func_name)
		shell_var := C.find_function(func_getter)
		word_list := NewWordList(func_getter)

		AppendWordList(&word_list, self.name)
		AppendWordList(&word_list, self.value)

		if shell_var != nil {
			C.execute_shell_function(shell_var, word_list)
		}

		C.free(unsafe.Pointer(func_getter))
		FreeWordList(word_list)
	}

	return self
}

// End FFI functions

type dynamicRegisterCMD struct {
	Name     string `short:"n" long:"name" description:"Name of the variable" required:"true"`
	Function string `short:"f" long:"function" description:"Function to call to get the value" required:"true"`
}

func (x *dynamicRegisterCMD) Execute(args []string) error {
	if x.Name == "" {
		return fmt.Errorf("name is required")
	}

	if x.Function == "" {
		return fmt.Errorf("function is required")
	}

	name := C.CString(x.Name)
	dynamicVars[x.Name] = &DynamicVars{
		variable: x.Name,
		function: x.Function,
	}

	defer C.free(unsafe.Pointer(name))
	C.create_dynamic_var(name, nil, (*[0]byte)(C.dynamicvar_get_wrapper), (*[0]byte)(C.dynamicvar_assign_wrapper))

	return nil
}

type dynamicUnregisterCMD struct {
	Name string `short:"n" long:"name" description:"Name of the variable" required:"true"`
}

func (x *dynamicUnregisterCMD) Execute(args []string) error {
	if x.Name == "" {
		return fmt.Errorf("name is required")
	}

	name := C.CString(x.Name)
	delete(dynamicVars, x.Name)
	C.free(unsafe.Pointer(name))

	return nil
}

type dynamicSetCMD struct {
	Name  string `short:"n" long:"name" description:"Name of the variable" required:"true"`
	Value string `short:"v" long:"value" description:"Value to set" required:"true"`
}

func (x *dynamicSetCMD) Execute(args []string) error {
	if x.Name == "" {
		return fmt.Errorf("name is required")
	}

	if x.Value == "" {
		return fmt.Errorf("value is required")
	}

	name := C.CString(x.Name)
	value := C.CString(x.Value)

	C.bind_variable(name, value, 1)

	C.free(unsafe.Pointer(name))
	C.free(unsafe.Pointer(value))

	return nil
}

type DynamicVarCMD struct {
	Register   dynamicRegisterCMD   `command:"register" description:"Register a dynamic variable"`
	Unregister dynamicUnregisterCMD `command:"unregister" description:"Unregister a dynamic variable"`
	Set        dynamicSetCMD        `command:"set" description:"Set a dynamic variable"`
}
