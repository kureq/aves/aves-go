#include <builtins.h>
#include <command.h>
#include <variables.h>

void create_dynamic_var(char *varname, char *val,
                        SHELL_VAR *(*get_func)(SHELL_VAR *),
                        SHELL_VAR *(*assign_func)(SHELL_VAR *, char *, long,
                                                  char *));
