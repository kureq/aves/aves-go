#include "ffi.h"

#include <variables.h>

char *aves_empty_doc[] = {NULL};

// Aves Builtins
extern int aves_builtin(WORD_LIST *list);
extern SHELL_VAR *dynamicvar_assign(SHELL_VAR *, char *, long, char *);
extern SHELL_VAR *dynamicvar_get(SHELL_VAR *);

SHELL_VAR *dynamicvar_assign_wrapper(SHELL_VAR *self, char *value, long unused,
                                     char *key) {
  return dynamicvar_assign(self, value, unused, key);
}

SHELL_VAR *dynamicvar_get_wrapper(SHELL_VAR *self) {
  return (dynamicvar_get(self));
}

struct builtin aves_struct = {"aves",
                              aves_builtin,
                              BUILTIN_ENABLED,
                              aves_empty_doc,
                              "Aves general function builtins",
                              0};
