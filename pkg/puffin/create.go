package puffin

import (
	"fmt"

	"gitlab.com/kureq/aves/aves-go/lib/conure"
)

type createCMD struct {
	Puffin string `short:"p" long:"puffin" description:"Register the puffin for repeated used" required:"true"`
	Type   string `short:"t" long:"type" description:"Type of the input (yaml, json, toml) default is yaml" default:"yaml" choice:"yaml" choice:"json" choice:"toml"`
}

func (x *createCMD) Execute(args []string) error {
	if x.Puffin == "" {
		return fmt.Errorf("you must provide a puffin name, to register the content to")
	}

	if _, ok := Puffins[x.Puffin]; !ok {
		Puffins[x.Puffin] = &Puffin{
			Conure: conure.New(),
			Type:   x.Type,
			Name:   x.Puffin,
		}
		return nil
	}

	return fmt.Errorf("puffin already exists")
}
