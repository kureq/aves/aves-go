package puffin

import (
	"fmt"

	"gitlab.com/kureq/aves/aves-go/lib/conure"
	"gitlab.com/kureq/aves/aves-go/lib/conure/merge"
	"gitlab.com/kureq/aves/aves-go/lib/conure/providers/raw"
	"gitlab.com/kureq/aves/aves-go/lib/ffi"
)

type getCMD struct {
	Puffin string `short:"p" long:"puffin" description:"Puffin to get the configuration from"`
	Output string `short:"o" long:"output" description:"Format the output"`
	Format string `short:"f" long:"format" description:"Format the output as a specific file type (json, yaml, toml)"`
	Input  struct {
		Input       string `short:"i" long:"input" description:"Input variable or value"`
		MergeArrays bool   `long:"merge-arrays" description:"Merge arrays instead of replacing them"`
		Strict      bool   `long:"strict" description:"Enable strict mode"`
	} `group:"Input"`
	Variables []string `short:"v" long:"variable" description:"Set BASH variable with the result"`
	Args      struct {
		Queries []string `positional-arg:"yes" required:"yes" description:"Input queries"`
	} `positional-args:"yes" required:"yes"`
	Error bool `short:"e" long:"error" description:"Return an error if the variable is not found"`
	Split bool `short:"s" long:"split" description:"Split maps into separate variables"`
	Test  bool `short:"t" long:"test" description:"Test mode"`
}

func (p *getCMD) Execute(args []string) error {
	if p.Test {
		return nil
	}

	if p.Puffin == "" && p.Input.Input == "" {
		return fmt.Errorf("puffin specifier or input variable is required")
	}

	if p.Puffin != "" && p.Input.Input != "" {
		return fmt.Errorf("puffin specifier and input variable are mutually exclusive")
	}

	var con *conure.Conure

	var parser conure.Parser

	if p.Format != "" {
		parser = GetParser(p.Format)
	}

	if p.Puffin != "" {
		if c, ok := Puffins[p.Puffin]; ok {
			con = c.Conure
		} else {
			return fmt.Errorf("puffin %s not found", p.Puffin)
		}
	}

	if p.Input.Input != "" {
		con = conure.New()

		opts := merge.Config{
			Strict:       p.Input.Strict,
			AppendArrays: p.Input.MergeArrays,
		}
		if input, err := ffi.Get(p.Input.Input); err == nil {
			if parser == nil {
				parser = GetParser("yaml")
			}

			if err := con.Load(raw.Provider([]byte(input)), parser, opts); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("could not read input")
		}
	}

	out := []string{}

	getOps := conure.GetOptions{
		Parser: parser,
		Split:  p.Split,
		Array:  false,
	}

	for _, query := range p.Args.Queries {
		values := con.Strings(query, getOps)
		if values == nil {
			continue
		}

		out = append(out, values...)
	}

	if len(out) == 0 && p.Error {
		return fmt.Errorf("no values found for: %s", p.Args.Queries)
	}

	if len(p.Variables) == 0 {
		for _, o := range out {
			fmt.Printf("%v\n", o)
		}
	} else {
		if len(out) > len(p.Variables) {
			if len(p.Variables) == 1 {
				ffi.SetArray(p.Variables[0], out)
			} else {
				remaining := out[len(p.Variables):]
				ffi.SetArray(p.Variables[len(p.Variables)-1], remaining)
				for i, o := range out[:len(p.Variables)] {
					ffi.Set(p.Variables[i], o)
				}
			}
		} else if len(out) == len(p.Variables) {
			for i, o := range out {
				ffi.Set(p.Variables[i], o)
			}
		} else {
			for i, o := range out {
				ffi.Set(p.Variables[i], o)
			}
			for i := len(out); i < len(p.Variables); i++ {
				ffi.Set(p.Variables[i], "")
			}
		}
	}

	return nil
}
