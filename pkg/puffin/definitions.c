#include "../../lib/ffi/ffi.h"

char *puffin_empty_doc[] = {NULL};

// Puffin Builtins
extern int puffin_builtin(WORD_LIST *list);

struct builtin puffin_struct = {"puffin",
                                puffin_builtin,
                                BUILTIN_ENABLED,
                                puffin_empty_doc,
                                "File and Variable manipulation",
                                0};
