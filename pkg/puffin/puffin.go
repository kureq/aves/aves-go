package puffin

import (
	"github.com/jessevdk/go-flags"
	"gitlab.com/kureq/aves/aves-go/lib/conure"
	"gitlab.com/kureq/aves/aves-go/lib/conure/parsers/json"
	"gitlab.com/kureq/aves/aves-go/lib/conure/parsers/toml"
	yaml "gitlab.com/kureq/aves/aves-go/lib/conure/parsers/yaml"
)

type Puffin struct {
	Conure *conure.Conure
	Type   string
	Name   string
}

var (
	Puffins map[string]*Puffin
	parsers map[string]conure.Parser

	pufftions     PuffinCLI // Get it.. cause it's a puffin, and it's options
	puffin_parser *flags.Parser
)

func GetParser(t string) conure.Parser {
	if p, ok := parsers[t]; ok {
		return p
	}

	switch t {
	case "toml":
		parsers[t] = toml.New()
	case "json":
		parsers[t] = json.New()
	case "yaml":
		fallthrough
	default:
		parsers[t] = yaml.New()
	}

	return parsers[t]
}

type PuffinCLI struct {
	Create  createCMD   `command:"create" description:"Create a new puffin"`
	Set     setCMD      `command:"set" description:"Set a value in a puffin"`
	Iterate iteratorCMD `command:"iterate" description:"Iterate over the puffin"`
	Read    readCMD     `command:"read" description:"Read from a file or input"`
	Get     getCMD      `command:"get" description:"Get a value from a puffin"`
	Forget  forgetCMD   `command:"forget" description:"Forget a value from a puffin"`
}
