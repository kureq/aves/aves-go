package puffin

import (
	"os"

	"gitlab.com/kureq/aves/aves-go/lib/conure"
	"gitlab.com/kureq/aves/aves-go/lib/conure/merge"
	"gitlab.com/kureq/aves/aves-go/lib/conure/providers/raw"
	"gitlab.com/kureq/aves/aves-go/lib/ffi"
)

type readCMD struct {
	Puffin string `short:"p" long:"puffin" description:"Register the puffin for repeated used" required:"true"`
	Type   string `short:"t" long:"type" description:"Type of the input" default:"yaml" choice:"yaml" choice:"json" choice:"toml"`
	Args   struct {
		Input string `positional-arg:"yes" required:"yes" description:"Input file or value"`
	} `positional-args:"yes" required:"yes"`
	MergeArrays bool `short:"m" long:"merge-arrays" description:"Merge arrays instead of replacing them"`
	Strict      bool `short:"s" long:"strict" description:"Enable strict mode"`
	Render      bool `short:"r" long:"render" description:"Render the input as a GO template before parsing"`
}

func (p *readCMD) Execute(args []string) error {
	conf, ok := Puffins[p.Puffin]

	opts := merge.Config{
		Strict:       p.Strict,
		AppendArrays: p.MergeArrays,
	}

	if !ok {
		Puffins[p.Puffin] = &Puffin{
			Conure: conure.New(),
			Type:   p.Type,
			Name:   p.Puffin,
		}

		conf = Puffins[p.Puffin]
	}

	if _, err := os.Stat(p.Args.Input); err == nil {
		if err := conf.Conure.LoadFile(p.Args.Input, GetParser(p.Type), opts); err != nil {
			return err
		}
	} else if input, err := ffi.Get(p.Args.Input); err == nil {
		if err = conf.Conure.Load(raw.Provider([]byte(input)), GetParser(p.Type), opts); err != nil {
			return err
		}
	}

	return nil
}
