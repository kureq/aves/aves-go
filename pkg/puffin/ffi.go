package puffin

/*
#cgo pkg-config: bash
#cgo linux LDFLAGS: -Wl,-unresolved-symbols=ignore-all
#cgo darwin LDFLAGS: -Wl,-undefined,dynamic_lookup
*/
import "C"

import (
	"fmt"
	"unsafe"

	"github.com/jessevdk/go-flags"
	"gitlab.com/kureq/aves/aves-go/lib/conure"
	"gitlab.com/kureq/aves/aves-go/lib/ffi"
)

//export puffin_builtin
func puffin_builtin(args unsafe.Pointer) C.int {
	pufftions = PuffinCLI{}
	vargs := ffi.ConvertWordList((ffi.WORD_LIST_PTR)(args))

	// PERF(VBE): Ironically enough the ParseArgs adds a lot of overhead
	//(avg ~14.9µs 1m runs - MAC M1) (Using the good ol' time.Since() to measure 🐒)
	if _, err := puffin_parser.ParseArgs(vargs); err != nil {
		switch flagsErr := err.(type) {
		case *LoopStopError:
			return C.int(flagsErr.StatusCode)
		case flags.ErrorType:
			if flagsErr == flags.ErrHelp {
				return C.int(0)
			}
			return C.int(1)
		default:
			fmt.Println(err)
			return C.int(1)
		}
	}

	return C.int(0)
}

//export puffin_builtin_load
func puffin_builtin_load(_ *C.char) C.int {
	puffin_parser = flags.NewParser(&pufftions, flags.HelpFlag|flags.PassDoubleDash)
	puffin_parser.SubcommandsOptional = false
	puffin_parser.Name = "puffin"

	Puffins = make(map[string]*Puffin)
	parsers = make(map[string]conure.Parser)
	return C.int(1)
}

//export puffin_builtin_unload
func puffin_builtin_unload(_ *C.char) C.int {
	// Clean Puffin array
	// Puffins = make(map[string]*Puffin)
	return 0
}
