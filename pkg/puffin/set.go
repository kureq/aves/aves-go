package puffin

import (
	"github.com/cstockton/go-conv"
)

type setCMD struct {
	Puffin string `short:"p" long:"puffin" description:"Puffin to get the configuration from"`
	Key    string `short:"k" long:"key" description:"Key to set the value to" required:"true"`
	Type   string `short:"t" long:"type" description:"Type of the value" choice:"bool" choice:"int" choice:"float" choice:"array" choice:"string" default:"string"`
	Args   struct {
		Values []string `required:"yes" description:"Value to set"`
	} `positional-args:"yes" required:"yes"`
}

func (p *setCMD) Execute(args []string) error {
	puffin := Puffins[p.Puffin]

	value, err := func() (interface{}, error) {
		value := p.Args.Values
		switch p.Type {
		case "bool":
			return conv.Bool(value)
		case "int":
			return conv.Int(value)
		case "float":
			return conv.Float64(value)
		case "array":
			return value, nil
		case "string":
			fallthrough
		default:
			v, er := conv.String(value)
			if er != nil {
				return nil, er
			}
			v = v[1 : len(v)-1]
			return v, nil
		}
	}()
	if err != nil {
		return err
	}

	_, err = puffin.Conure.Set(p.Key, value)
	if err != nil {
		return err
	}

	if err != nil {
		return err
	}

	return nil
}
