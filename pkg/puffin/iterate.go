package puffin

import (
	"errors"

	"gitlab.com/kureq/aves/aves-go/lib/conure"
	"gitlab.com/kureq/aves/aves-go/lib/conure/merge"
	"gitlab.com/kureq/aves/aves-go/lib/conure/providers/raw"
	"gitlab.com/kureq/aves/aves-go/lib/ffi"
)

type LoopStopError struct {
	Err        error
	StatusCode int
}

func (r *LoopStopError) Error() string {
	return r.Err.Error()
}

var iterators = map[string]func() int{}

func puffinIterator(variables []string, args []string) func() int {
	n := 0
	vars := variables
	nargs := args

	return func() int {
		for i := 0; i < len(vars); i++ {
			if n < len(nargs) {
				ffi.Set(vars[i], nargs[n])
				n++
			} else {
				ffi.Set(vars[i], "")
				return -1
			}
		}

		return n
	}
}

type iteratorCMD struct {
	Puffin    string   `short:"p" long:"puffin" description:"Puffin to iterate over" required:"true"`
	Input     string   `short:"i" long:"input" description:"Input variable or value"`
	Name      string   `short:"n" long:"name" description:"Name of the iterator" required:"true"`
	Format    string   `short:"f" long:"format" description:"Format the output as a specific file type (json, yaml, toml)"`
	Variables []string `short:"v" long:"variable" description:"Set BASH variable with the result"`
	Args      struct {
		Queries []string `positional-arg:"yes" required:"yes" description:"Input queries"`
	} `positional-args:"yes" required:"yes"`
	Split bool `short:"s" long:"split" description:"Split input into array"`
	Array bool `short:"a" long:"array" description:"Return Array?"`
}

func (p *iteratorCMD) Execute(args []string) error {
	if ite, ok := iterators[p.Name]; ok {
		n := ite()
		if n == -1 {
			delete(iterators, p.Name)
			return &LoopStopError{
				Err:        errors.New("no more items"),
				StatusCode: 1,
			}
		}
	} else {
		var parser conure.Parser
		var con *conure.Conure

		if p.Format != "" {
			parser = GetParser(p.Format)
		}

		if p.Puffin != "" {
			if conf, ok := Puffins[p.Puffin]; ok {
				con = conf.Conure
			} else {
				return errors.New("puffin " + p.Puffin + " not found")
			}
		}

		if p.Input != "" {
			con = conure.New()

			opts := merge.Config{
				Strict:       false,
				AppendArrays: false,
			}
			if input, err := ffi.Get(p.Input); err == nil {
				if parser == nil {
					parser = GetParser("yaml")
				}

				if err := con.Load(raw.Provider([]byte(input)), parser, opts); err != nil {
					return err
				}
			} else {
				return errors.New("input not found")
			}
		}

		getOptions := conure.GetOptions{
			Parser: parser,
			Split:  p.Split,
			Array:  p.Array,
		}

		vars := con.Strings(p.Args.Queries[0], getOptions)
		iterators[p.Name] = puffinIterator(p.Variables, vars)

		n := iterators[p.Name]()

		if n == -1 {
			delete(iterators, p.Name)
			return &LoopStopError{
				Err:        errors.New("no more items"),
				StatusCode: 1,
			}
		}
	}
	return nil
}
