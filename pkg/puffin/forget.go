package puffin

import (
	"fmt"
)

type forgetCMD struct{}

func (x *forgetCMD) Execute(args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("keys are required")
	}

	for _, k := range args {
		delete(Puffins, k)
	}

	return nil
}
